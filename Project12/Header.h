#pragma once
#include <string>
using namespace std;
class Graph
{

protected:
	int **a; //pointer to the matrix
	int n; //number of vertex
	int timer;
	bool *used;
	int *tin;
	int *tout;
	int **a_cost;//��������� �� ������� ���������
	int *cost_arr = new int[n];
	int cost = 0;
	void dfs(int i, int prev = -1);
    int min = 999;
	int least(int c);
	void minCost(int city);

public:
	Graph(int n);
	Graph::Graph(){}
	int getSize() { return n; }
	static Graph * loadFromMarixFile(const string filePath);
	static Graph * loadFromEdgeListFile(const string filePath);
	static Graph * loadFromDimaxFormat(const string filePath);

	void printMatrix();
	void printCostMatrix();
	void loadCostMatrix();
	void find_bridges();
	int TSR();
	int TSR2();
	void MinPath(int i);
	void dfs_TSR(int i,int prev=-1);
	int MST();
	int getNumberOfComponent();

	Graph getMaxSpaningTree();

	bool isIsomorphic(Graph & otherGraph);

	~Graph(void);
};


