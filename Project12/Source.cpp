#include "Header.h"
#include <iostream>
#include "fstream"
#include <vector>
#include <algorithm>
#define ERROR(e) {cout << "ERROR: " << e << endl; exit(1);}
Graph::Graph(int n)
{
	this->n = n;
	a = new int *[n];
	for (int i = 0; i < n; i++) {
		a[i] = new int[n];
		for (int j = 0; j < n; j++)
			a[i][j] = 0;
	}
}

Graph * Graph::loadFromMarixFile(const string filePath)
{
	int graphSize;
	ifstream stream(filePath);
	stream >> graphSize;

	Graph * newGraph = new Graph(graphSize);

	return newGraph;
}

Graph * Graph::loadFromDimaxFormat(const string filePath)
{
	int graphSize = 0;
	ifstream stream(filePath);


	stream >> graphSize;

	Graph * newGraph = new Graph(graphSize);

	return newGraph;
}


Graph * Graph::loadFromEdgeListFile(const string filePath) {
	int    graphSize;
	ifstream stream(filePath);

	if (!stream.is_open()) {
		ERROR("Can not open input file: " + filePath);
	}

	stream >> graphSize;

	Graph * newGraph = new Graph(graphSize);

	int i, j;
	while (!stream.eof()) {
		stream >> i >> j;
		newGraph->a[i - 1][j - 1] = 1;
		newGraph->a[j - 1][i - 1] = 1;
	}

	return newGraph;
}

Graph::~Graph(void)
{
	for (int i = 0; i < n; i++) {
		delete a[i];
	}

	delete a;
}


void Graph::printMatrix() {
	cout << "Size: " << n << "\n";


	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++)
			cout << a[i][j] << "\t";
		cout << "\n";
	}


}
void Graph::find_bridges()
{
	timer = 0;
	this->used = new bool[n];
	this->tin = new int[n];
	this->tout = new int[n];
	for (int i = 0; i < n; ++i)
	{
		used[i] = false;
	}
	for (int i = 0; i < n; ++i)
	{
		if (!used[i])
		{
			dfs(i);
		}
	}
}
void Graph::dfs(int i, int prev)
{
	used[i] = true;
	tin[i] = tout[i] = timer++;
	for (int j = 0; j < n; ++j)
	{
		if (a[i][j] == 1)
		{
			int to = j;
			if (to == prev)  continue;
			if (used[to]) tout[i] = std::min(tout[i], tin[to]);
			else
			{
				dfs(to, i);
				tout[i] = std::min(tout[i], tout[to]);
				if (tout[to] > tin[i]) std::cout << "bridge: (" << i+1 << " , " << to+1 << ")\n";
			}
		}
	}
}
void Graph::printCostMatrix()
{
	cout << "Size: " << n << "\n";
	for (int i = 0; i < n; i++)
	{
		cout << "\n";

		for (int j = 0; j < n; j++)
			cout << "\t" << a_cost[i][j];
	}
}
void Graph::loadCostMatrix()
{
	ifstream in("cost.txt");
	if (in.is_open())
	{
		for (int i = 0; i < n; i++)
		{
			for (int j = 0; j < n; j++)
			{
				in >> a_cost[i][j];
			}
			used[i] = false;
		}
			
	}
	else
	{
		cout << "File did not found" << endl;
		return;
	}
}
int Graph::least(int c)
{
	int  n_c = 999;
	int min = 9999, k_min;

	for (int i = 0; i < n; i++)
	{
		if ((a_cost[c][i] != 0) && (used[i] == false))
			if (a_cost[c][i] + a_cost[i][c] < min)
			{
				min = a_cost[i][0] + a_cost[c][i];
				k_min = a_cost[c][i];
				n_c = i;
			}
	}
	
	if (min != 9999)
	{
		cost += k_min;
	//	cout << cost << endl;
	}

	return n_c;
}
void Graph::minCost(int city)
{
	int i, n_city;

	used[city] = true;

	cout << city + 1 << "--->";
	n_city = least(city);

	if (n_city == 999)
	{
		n_city = 0;
		cout << n_city + 1;
		cost += a_cost[city][n_city];

		return;
	}

	minCost(n_city);
}
int Graph::TSR()
{
		used = new bool[n];
		a_cost = new int *[n];
		for (int i = 0; i < n; i++)
		{
			a_cost[i] = new int[n];
		}
		loadCostMatrix();
		printCostMatrix();
		cout << "\n\nThe Path is:\n";
		minCost(0);
		cout << "\nMinimum cost is " << cost;

		return 0;
}
int Graph::MST()
{
	used = new bool[n];
	a_cost = new int *[n];
	for (int i = 0; i < n; i++)
	{
		a_cost[i] = new int[n];
	}
	loadCostMatrix();
	printCostMatrix();
	return 1;
}
void Graph::MinPath(int i)
{
	
}
static int flag = 0;
int Graph::TSR2()
{
	used = new bool[n];
	
	cost = 0;
	a_cost = new int *[n];
	for (int i = 0; i < n; i++)
	{
		a_cost[i] = new int[n];
	}
	for (int i = 0; i < n; ++i)
	{
		used[i] = false;
	}
	loadCostMatrix();
	printCostMatrix();
	cout << "\n";
	for (int i = 0; i < n; ++i)
	{
		
		if (!used[i])
		{
			dfs_TSR(i);
		}
		for (int i = 0; i < n; ++i)
		{
			used[i] = false;
		}
	}
	cout << min;
	
	return 0;
}

void Graph::dfs_TSR(int i, int prev)
{
	
	used[i] = true;
	for (int j = 0; j < n; ++j)
	{
		if (a_cost[i][j] !=0)
		{
			if (j == prev)
			{
				continue;
			}
			else if (used[j])
			{
				 continue;
			}
			else
			{
				cost += a_cost[i][j];
				dfs_TSR(j, i);
				cout << cost;
				if (cost< min&& cost!=0)
				{
					min = cost+ a_cost[i][j];
				}
				
				cost = 0;
				
				//cost_arr[i] + a_cost[0][i];
				//if (cost_arr[i] > 0) cout << cost_arr[j] + a_cost[0][j]<< endl;
			}
		}
	}
	
}